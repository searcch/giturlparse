from .base import BasePlatform


class GitHubPlatform(BasePlatform):
    PATTERNS = {
        "https": (
            r"(?P<protocols>(git\+)?(?P<protocol>https))://(?P<domain>[^/]+?)"
            r"(?P<pathname>/(?P<owner>[^/]+?)/(?P<repo>[^/]+?)(?:\.git)?(?P<path_raw>(/blob/|/tree/|/commit/).+)?)$"
        ),
        "ssh": (
            r"(?P<protocols>(git\+)?(?P<protocol>ssh))?(://)?git@(?P<domain>.+?)(?P<pathname>(:|/)"
            r"(?P<owner>[^/]+)/(?P<repo>[^/]+?)(?:\.git)"
            r"(?P<path_raw>(/blob/|/tree/|/commit/).+)?)$"
        ),
        "git": (
            r"(?P<protocols>(?P<protocol>git))://(?P<domain>.+?)"
            r"(?P<pathname>/(?P<owner>[^/]+)/(?P<repo>[^/]+?)(?:\.git)?"
            r"(?P<path_raw>(/blob/|/tree/|/commit/).+)?)$"
        ),
    }
    FORMATS = {
        "https": r"https://%(domain)s/%(owner)s/%(repo)s%(path_raw)s",
        "ssh": r"git@%(domain)s:%(owner)s/%(repo)s.git%(path_raw)s",
        "git": r"git://%(domain)s/%(owner)s/%(repo)s.git%(path_raw)s",
    }
    CLONE_FORMATS = {
        "https": r"https://%(domain)s/%(owner)s/%(repo)s",
        "ssh": r"git@%(domain)s:%(owner)s/%(repo)s.git",
        "git": r"git://%(domain)s/%(owner)s/%(repo)s.git",
    }
    DOMAINS = (
        "github.com",
        "gist.github.com",
    )
    DEFAULTS = {"_user": "git"}

    @staticmethod
    def clean_data(data):
        data = BasePlatform.clean_data(data)
        data["isfile"] = False
        data["isdir"] = False
        if data["path_raw"].startswith("/blob/"):
            pr = data["path_raw"].replace("/blob/", "")
            prl = list(filter(lambda x: bool(x),pr.split("/")))
            data["path"] = prl[0]
            if len(prl) > 1:
                data["subpath"] = "/".join(prl[1:])
            data["isfile"] = True
        elif data["path_raw"].startswith("/tree/"):
            pr = data["path_raw"].replace("/tree/", "")
            prl = list(filter(lambda x: bool(x),pr.split("/")))
            data["branch"] = prl[0]
            if len(prl) > 1:
                data["subpath"] = "/".join(prl[1:])
                data["isdir"] = True
        elif data["path_raw"].startswith("/commit/"):
            pr = data["path_raw"].replace("/commit/", "")
            prl = list(filter(lambda x: bool(x),pr.split("/")))
            data["commit"] = prl[0]
        return data
