from .base import BasePlatform


class GitLabPlatform(BasePlatform):
    PATTERNS = {
        "https": (
            r"(?P<protocols>(git\+)?(?P<protocol>https))://(?P<domain>.+?)(?P<port>:[0-9]+)?"
            r"(?P<pathname>/(?P<owner>[^/]+?)/"
            r"(?P<groups_path>.*?)?(?(groups_path)/)?(?P<repo>[^/]+?)(?:\.git)?"
            r"(?P<path_raw>(/-/blob/|/-/tree/|/-/commit/|/-/tags/).+))$"
        ),
        "ssh": (
            r"(?P<protocols>(git\+)?(?P<protocol>ssh))?(://)?git@(?P<domain>.+?):(?P<port>[0-9]+)?(?(port))?"
            r"(?P<pathname>/?(?P<owner>[^/]+)/"
            r"(?P<groups_path>.*?)?(?(groups_path)/)?(?P<repo>[^/]+?)(?:\.git)?"
            r"(?P<path_raw>(/-/blob/|/-/tree/|/-/commit/|/-/tags/).+))$"
        ),
        "git": (
            r"(?P<protocols>(?P<protocol>git))://(?P<domain>.+?):(?P<port>[0-9]+)?(?(port))?"
            r"(?P<pathname>/?(?P<owner>[^/]+)/"
            r"(?P<groups_path>.*?)?(?(groups_path)/)?(?P<repo>[^/]+?)(?:\.git)?"
            r"(?P<path_raw>(/-/blob/|/-/tree/|/-/commit/|/-/tags/).+))$"
        ),
    }
    FORMATS = {
        "https": r"https://%(domain)s/%(owner)s/%(groups_slash)s%(repo)s.git%(path_raw)s",
        "ssh": r"git@%(domain)s:%(port_slash)s%(owner)s/%(groups_slash)s%(repo)s.git%(path_raw)s",
        "git": r"git://%(domain)s%(port)s/%(owner)s/%(groups_slash)s%(repo)s.git%(path_raw)s",
    }
    CLONE_FORMATS = {
        "https": r"https://%(domain)s/%(owner)s/%(groups_slash)s%(repo)s.git",
        "ssh": r"git@%(domain)s:%(port_slash)s%(owner)s/%(groups_slash)s%(repo)s.git",
        "git": r"git://%(domain)s%(port)s/%(owner)s/%(groups_slash)s%(repo)s.git",
    }
    SKIP_DOMAINS = (
        "github.com",
        "gist.github.com",
    )
    DEFAULTS = {"_user": "git", "port": ""}

    @staticmethod
    def clean_data(data):
        data = BasePlatform.clean_data(data)
        data["isfile"] = False
        data["isdir"] = False
        if data["path_raw"].startswith("/-/blob/"):
            pr = data["path_raw"].replace("/-/blob/", "")
            prl = list(filter(lambda x: bool(x),pr.split("/")))
            data["path"] = prl[0]
            if len(prl) > 1:
                data["subpath"] = "/".join(prl[1:])
            data["isfile"] = True
        elif data["path_raw"].startswith("/-/tree/"):
            pr = data["path_raw"].replace("/-/tree/", "")
            prl = list(filter(lambda x: bool(x),pr.split("/")))
            data["branch"] = prl[0]
            if len(prl) > 1:
                data["subpath"] = "/".join(prl[1:])
                data["isdir"] = True
        elif data["path_raw"].startswith("/-/commit/"):
            pr = data["path_raw"].replace("/-/commit/", "")
            prl = list(filter(lambda x: bool(x),pr.split("/")))
            data["commit"] = prl[0]
        elif data["path_raw"].startswith("/-/tags/"):
            pr = data["path_raw"].replace("/-/tags/", "")
            prl = list(filter(lambda x: bool(x),pr.split("/")))
            # Emulate github, which does not use /tags/.
            data["branch"] = prl[0]
        return data
