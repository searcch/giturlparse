import itertools
import re
from copy import copy

class BasePlatform:
    FORMATS = {
        "ssh": r"ssh://%(_user)s@%(domain)s:%(repo)s.git",
        "http": r"http://%(domain)s/%(repo)s%(git_postfix)s",
        "https": r"https://%(domain)s/%(repo)s%(git_postfix)s",
        "git": r"git://%(domain)s/%(repo)s.git",
    }
    CLONE_FORMATS = copy(FORMATS)

    PATTERNS = {
        "ssh": r"(?P<_user>.+)@(?P<domain>[^/]+?):(?P<repo>.+)(?P<git_postfix>\.git)",
        "http": r"http://(?P<domain>[^/]+?)/(?P<repo>((.+)(?=\.git)|(.+)))(?P<git_postfix>\.git)?",
        "https": r"https://(?P<domain>[^/]+?)/(?P<repo>((.+)(?=\.git)|(.+)))(?P<git_postfix>\.git)?",
        "git": r"git://(?P<domain>[^/]+?)/(?P<repo>.+)(?P<git_postfix>\.git)",
    }

    # None means it matches all domains
    DOMAINS = None
    SKIP_DOMAINS = None
    DEFAULTS = {}

    def __init__(self):
        # Precompile PATTERNS
        self.COMPILED_PATTERNS = {proto: re.compile(regex, re.IGNORECASE) for proto, regex in self.PATTERNS.items()}

        # Supported protocols
        self.PROTOCOLS = self.PATTERNS.keys()

        if self.__class__ == BasePlatform:
            sub = [subclass.SKIP_DOMAINS for subclass in self.__class__.__subclasses__() if subclass.SKIP_DOMAINS]
            if sub:
                self.SKIP_DOMAINS = list(itertools.chain.from_iterable(sub))

    @staticmethod
    def clean_data(data):
        data["path"] = ""
        data["branch"] = ""
        if not "git_postfix" in data:
            data["git_postfix"] = ""
        if "protocols" in data:
            data["protocols"] = list(filter(lambda x: x, data["protocols"].split("+")))
        else:
            data["protocols"] = ""
        if "pathname" in data:
            data["pathname"] = data["pathname"].strip(":")
        else:
            data["pathname"] = ""
        return data
